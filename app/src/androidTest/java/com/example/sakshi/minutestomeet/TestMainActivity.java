package com.example.sakshi.minutestomeet;

import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Sakshi on 12/15/2015.
 */
public class TestMainActivity extends ActivityInstrumentationTestCase2<MainActivity>{

        MainActivity mainActivity;
        public TestMainActivity() {
            super(MainActivity.class);
        }

        @Override
        protected void setUp() throws Exception {
            super.setUp();
            mainActivity = getActivity();

            getInstrumentation().waitForIdleSync();



        }

        @Override
        protected void tearDown() throws Exception {
            super.tearDown();
        }

       // @Test
        public void testEmailViewNotNull(){
            TextView textView = (TextView)mainActivity.findViewById(R.id.textViewEmail);
            assertNotNull(textView);
        }

        //@Test
        public void testPasswordViewNotNull(){
            TextView textView = (TextView)mainActivity.findViewById(R.id.textViewPassword);
            assertNotNull(textView);
        }

        //@Test
        public void testLoginButtonViewNotNull(){
            Button textView = (Button)mainActivity.findViewById(R.id.buttonLogin);
            assertNotNull(textView);
        }

        public void testImageViewViewNotNull(){
            ImageView textView = (ImageView)mainActivity.findViewById(R.id.imageViewLogo);
            assertNotNull(textView);
        }

        public void testRegisterViewNotNull(){
            TextView textView = (TextView)mainActivity.findViewById(R.id.textViewRegister);
            assertNotNull(textView);
        }

        public void testRegisterViewVisible(){
            TextView textView = (TextView)mainActivity.findViewById(R.id.textViewRegister);
            assertEquals(textView.getVisibility(), View.VISIBLE);
        }

        public void testLoginButtonViewVisible(){
            Button textView = (Button)mainActivity.findViewById(R.id.buttonLogin);
            assertEquals(textView.getVisibility(), View.VISIBLE);
        }

}
