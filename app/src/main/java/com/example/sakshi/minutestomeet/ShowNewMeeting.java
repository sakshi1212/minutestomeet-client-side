//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ShowNewMeeting extends Activity {
    public final static String EXTRA_MESSAGE = "com.sakshi.minutestomeeet.MESSAGE";
    public static final String MY_PREFS_NAME = "MTMPrefsFile";
    Bundle bundle;
    String json_code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_new_meeting);

        bundle = getIntent().getExtras();
       // if (bundle!=null) {
      //      Log.d("sakshi", "bundle not null");
            String title = bundle.getString("title");
            ((TextView) findViewById(R.id.titleTextView)).setText(title);
     //   }
        String date = bundle.getInt("day") + "/" + bundle.getInt("month") + "/" + bundle.getInt("year");
        ((TextView)findViewById(R.id.dateTextView)).setText(date);
        String hour;
        if (Integer.toString(bundle.getInt("hour")).length()==1){
            hour = "0"+Integer.toString(bundle.getInt("hour"));
        }else{
            hour = Integer.toString(bundle.getInt("hour"));
        }
        String min;
        if (Integer.toString(bundle.getInt("minute")).length()==1){
            min = "0"+Integer.toString(bundle.getInt("minute"));
        }else{
            min = Integer.toString(bundle.getInt("minute"));
        }
        String time = hour +":"+ min+" (24 hour format)";
        ((TextView)findViewById(R.id.timeTextView)).setText(time);
        int count = bundle.getInt("count");
        String members="";
        for (int i = 1; i<=count; i++) {
            members = members+ bundle.getString("name"+i)+" ---> "+bundle.getString("msisdn"+i)+"\n";
        }
        ((TextView)findViewById(R.id.membersTextView)).setText(members);
        String location = "latitude :"+bundle.getDouble("lat")+"\n" +"longitude :"+bundle.getDouble("long")+"\n";
        Context mContext = getApplicationContext();
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(bundle.getDouble("lat"), bundle.getDouble("long"), 1);
            String locality = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            if (locality!=null){
                location = location +"locality :"+locality+"\n";
            }
            if (state!=null) {
                location =location + "state :"+state+"\n";
            }
            if(country!=null){
                location = location +"country :"+country;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        ((TextView)findViewById(R.id.locationTextView)).setText(location);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_new_meeting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendNewMeetingReq(View view){
        new Connection().execute();
    }
    private class Connection extends AsyncTask {

        @Override
        protected Object doInBackground(Object... arg0) {
            // Creating HTTP client
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = null;
            // Creating HTTP Post
            String address = getResources().getString(R.string.IPport)+"/meeting/new_meeting";
            HttpPost httpPost = new HttpPost(address);
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
            //get name and phone num from shared pref
            SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            String Iname = (sharedPreferences.getString("name", ""));
            String Imsisdn = (sharedPreferences.getString("msisdn", ""));
            nameValuePair.add(new BasicNameValuePair("Iname",Iname ));
            nameValuePair.add(new BasicNameValuePair("Imsisdn",Imsisdn));
            //add rest
            nameValuePair.add(new BasicNameValuePair("title", bundle.getString("title")));
            nameValuePair.add(new BasicNameValuePair("day", Integer.toString(bundle.getInt("day"))));
            nameValuePair.add(new BasicNameValuePair("month", Integer.toString(bundle.getInt("month"))));
            nameValuePair.add(new BasicNameValuePair("year", Integer.toString(bundle.getInt("year"))));
            String hour;
            if (Integer.toString(bundle.getInt("hour")).length()==1){
                hour = "0"+Integer.toString(bundle.getInt("hour"));
            }else{
                hour = Integer.toString(bundle.getInt("hour"));
            }
            nameValuePair.add(new BasicNameValuePair("hour", hour));
            String min;
            if (Integer.toString(bundle.getInt("minute")).length()==1){
                min = "0"+Integer.toString(bundle.getInt("minute"));
            }else{
                min = Integer.toString(bundle.getInt("minute"));
            }
            nameValuePair.add(new BasicNameValuePair("minute", min));
            nameValuePair.add(new BasicNameValuePair("count", Integer.toString(bundle.getInt("count"))));

            for (int i = 1; i<=(bundle.getInt("count")); i++) {
                nameValuePair.add(new BasicNameValuePair("name"+i, bundle.getString("name"+i)));
                nameValuePair.add(new BasicNameValuePair("msisdn"+i, bundle.getString("msisdn"+i)));
            }
            nameValuePair.add(new BasicNameValuePair("latitude", Double.toString(bundle.getDouble("lat"))));
            nameValuePair.add(new BasicNameValuePair("longitude", Double.toString(bundle.getDouble("long"))));

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
            } catch (UnsupportedEncodingException e) {
                // writing error to Log
                e.printStackTrace();
            }
            try {
                response = httpClient.execute(httpPost);
            } catch (ClientProtocolException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (IOException e) {
                // writing exception to log
                e.printStackTrace();
            }catch (Exception e) {
                e.printStackTrace();
            }

            String responseText = null;
            try {
                responseText = EntityUtils.toString(response.getEntity());
                // Convert String to json object
                JSONObject json = new JSONObject(responseText);
                // get json object if nested
                //JSONObject json_name = json.getJSONObject("name");
              json_code =json.getString("code");
//            Log.d("code : ",json_code);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            Log.d("RESPONSE CODE ", json_code);
            if (json_code.equals("0")) {
                Intent intent = new Intent(ShowNewMeeting.this, WelcomeActivity.class);
                intent.putExtra(EXTRA_MESSAGE, "Welcome ");
                Toast.makeText(ShowNewMeeting.this,"Internal server/database error. Try again.", Toast.LENGTH_LONG).show();
                startActivity(intent);
            }else {
                Intent intent = new Intent(ShowNewMeeting.this, WelcomeActivity.class);
                intent.putExtra(EXTRA_MESSAGE, "Welcome ");
                Toast.makeText(ShowNewMeeting.this, "New Meeting Created", Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        }

    }
}




