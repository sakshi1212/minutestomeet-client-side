//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MyMeetingsActivity extends Activity {
    public static final String MY_PREFS_NAME = "MTMPrefsFile";
    String json_title, json_i_name, json_time, json_date, json_loc_lat, json_loc_long, json_tot_mem;
    public final static String EXTRA_MESSAGE = "com.sakshi.minutestomeeet.MESSAGE";
    int idOfMeeting;
    String meetingResponse;
    String json_code, json_code2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_meetings);

        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("id");
        int pos = id.lastIndexOf("-") + 1;
        id = id.substring(pos);
        idOfMeeting = Integer.parseInt(id) - 10000;
        Log.d("Sakshi", Integer.toString(idOfMeeting));
        new Connection().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_meetings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class Connection extends AsyncTask {

        @Override
        protected Object doInBackground(Object... arg0) {
            // Creating HTTP client
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = null;
            // Creating HTTP Post
            String address = getResources().getString(R.string.IPport) + "/meeting/find_by_id";
            HttpPost httpPost = new HttpPost(address);
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
            nameValuePair.add(new BasicNameValuePair("id", Integer.toString(idOfMeeting)));
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
            } catch (UnsupportedEncodingException e) {
                // writing error to Log
                e.printStackTrace();
            }
            try {
                response = httpClient.execute(httpPost);
            } catch (ClientProtocolException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (IOException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String responseText = null;
            try {
                responseText = EntityUtils.toString(response.getEntity());
                // Convert String to json object
                JSONObject json = new JSONObject(responseText);
                // get json object if nested
                //JSONObject json_name = json.getJSONObject("name");
                json_title = json.getString("title");
                //Log.d("title ", json_title);
                json_i_name = json.getString("i_name");
                json_time = json.getString("time");
                json_date = json.getString("date");
                json_loc_lat = json.getString("loc_lat");
                json_loc_long = json.getString("loc_long");
                json_tot_mem = json.getString("tot_mem");
                //Log.d("code : ",json_code);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            ((TextView) findViewById(R.id.titleTextView)).setText(json_title);
            ((TextView) findViewById(R.id.iNameTextView)).setText(json_i_name);
            ((TextView) findViewById(R.id.dateTextView)).setText(json_date);
            ((TextView) findViewById(R.id.timeTextView)).setText(json_time + "(24 hr format)");
            String full_location = "Latitude: " + json_loc_lat + "\nLongitude:" + json_loc_long;

            Context mContext = getApplicationContext();
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            try {
                Double.parseDouble(json_loc_lat);
                List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(json_loc_lat), Double.parseDouble(json_loc_long), 1);
                String locality = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                if (locality != null) {
                    full_location = full_location + "\nLocality:" + locality;
                }
                if (state != null) {
                    full_location = full_location + "\nState:" + state;
                }
                if (country != null) {
                    full_location = full_location + "\nCountry:" + country;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            ((TextView) findViewById(R.id.locTextView)).setText(full_location);
            //check for cancel button
            SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            String Iname = (sharedPreferences.getString("name", ""));
            if (Iname.equalsIgnoreCase(json_i_name)) {//if user is the initiator
                //show cancelled button
                Log.d("Sakshi", Iname + json_i_name);
                ((Button) findViewById(R.id.cancelMeetingButton)).setVisibility(View.VISIBLE);
            }
        }

    }

    public void showLocInMap(View v) {
        Intent maps = new Intent(this, One_Location.class);
        maps.putExtra("calledfrom", "MyMeetings");
        maps.putExtra("lat", json_loc_lat);
        maps.putExtra("lon", json_loc_long);
        startActivity(maps);
    }

    public void viewParticipants(View view) {
        Intent participants = new Intent(this, ViewParticipants.class);
        participants.putExtra("meeting_id", idOfMeeting);
        participants.putExtra("meeting_lat", json_loc_lat);
        participants.putExtra("meeting_long", json_loc_long);
        startActivity(participants);
    }

    public void cancelMeeting(View view) {
        new Cancel().execute();
    }

    private class Cancel extends AsyncTask {

        @Override
        protected Object doInBackground(Object... arg0) {
            // Creating HTTP client
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = null;
            // Creating HTTP Post
            String address = getResources().getString(R.string.IPport) + "/meeting/cancel_meeting";
            HttpPost httpPost = new HttpPost(address);
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
            nameValuePair.add(new BasicNameValuePair("id", Integer.toString(idOfMeeting)));
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
            } catch (UnsupportedEncodingException e) {
                // writing error to Log
                e.printStackTrace();
            }
            try {
                response = httpClient.execute(httpPost);
            } catch (ClientProtocolException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (IOException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String responseText = null;
            try {
                responseText = EntityUtils.toString(response.getEntity());
                // Convert String to json object
                JSONObject json = new JSONObject(responseText);
                // get json object if nested
                //JSONObject json_name = json.getJSONObject("name");
                json_code2 = json.getString("code");

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (json_code2.equalsIgnoreCase("1")) {
                Intent intent = new Intent(MyMeetingsActivity.this, WelcomeActivity.class);
                intent.putExtra(EXTRA_MESSAGE, "Welcome ");
                Toast.makeText(MyMeetingsActivity.this, "Meeting cancelled successfully", Toast.LENGTH_SHORT).show();
                startActivity(intent);
            } else {
                Intent intent = new Intent(MyMeetingsActivity.this, WelcomeActivity.class);
                intent.putExtra(EXTRA_MESSAGE, "Welcome ");
                Toast.makeText(MyMeetingsActivity.this, "Internal Server Error. Try again later", Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        }

    }
}
