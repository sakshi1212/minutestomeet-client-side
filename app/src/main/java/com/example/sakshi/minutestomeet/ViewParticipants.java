//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ViewParticipants extends Activity {
    ArrayList<String> participantItems=new ArrayList<String>();
    ArrayAdapter<String> adapter;
    int idOfMeeting;
    String json_count, json_msisdn, json_ini, json_lat, json_long, json_name, json_acc;
    String meeting_lat="", meeting_long="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_participants);

        ListView list = (ListView)findViewById(R.id.partListView);
        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                participantItems);
        list.setAdapter(adapter);
        idOfMeeting = getIntent().getIntExtra("meeting_id", -1);
        meeting_lat = getIntent().getStringExtra("meeting_lat");
        meeting_long = getIntent().getStringExtra("meeting_long");
        Log.d("meeting_lat from VP",meeting_lat);
        Log.d("meeting_lat from VP",meeting_lat);
        new Connection().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_participants, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private class Connection extends AsyncTask {

        @Override
        protected Object doInBackground(Object... arg0) {
            // Creating HTTP client
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = null;
            // Creating HTTP Post
            String address = getResources().getString(R.string.IPport) + "/meeting/find_participants";
            HttpPost httpPost = new HttpPost(address);
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
            nameValuePair.add(new BasicNameValuePair("id", Integer.toString(idOfMeeting)));
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
            } catch (UnsupportedEncodingException e) {
                // writing error to Log
                e.printStackTrace();
            }
            try {
                response = httpClient.execute(httpPost);
            } catch (ClientProtocolException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (IOException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String responseText = null;
            try {
                responseText = EntityUtils.toString(response.getEntity());
                // Convert String to json object
                JSONObject json = new JSONObject(responseText);
                // get json object if nested
                json_count =json.getString("count");

                int count = Integer.parseInt(json_count);
                for (int i = 1; i<=count; i++) {
                    JSONObject json_obj = json.getJSONObject(Integer.toString(i));
                    json_name =json_obj.getString("name");
                    json_msisdn=json_obj.getString("msisdn");
                    json_ini=json_obj.getString("initiator");
                    json_lat=json_obj.getString("lat");
                    json_long=json_obj.getString("long");
                    String acceptance = json_obj.getString("accept"); //1 or 0 or -1
                    String text="";
                    Log.d("accept ", acceptance);
                    if(acceptance.equalsIgnoreCase("1")){
                        json_acc = "ACCEPTED";
                        text = new RespToMeeting().calcDur(json_lat, json_long);
                    }else if (json_acc.equalsIgnoreCase("0")){
                        json_acc = "DECLINED";
                        text = new RespToMeeting().calcDur(json_lat, json_long);
                    }else{
                        if (json_lat.equalsIgnoreCase("NA")||(json_long).equalsIgnoreCase("NA")){
                            json_acc = "NA - app not installed";
                            text = "\nDist. away from Meeting : NA" +"\nTime away from Meeting : NA";
                        }else {
                            json_acc = "NO RESPONSE YET";
                            text = new RespToMeeting().calcDur(json_lat, json_long);
                        }
                    }
                    //String text = new RespToMeeting().calcDur(json_lat, json_long);
                    String part = "Name :" + json_name + "\nPhone Num :" + json_msisdn + "\nAcceptance : " + json_acc+text;
                    participantItems.add(part);

                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            ((TextView)findViewById(R.id.totalTextView)).setText("Total Number of Participants are : " + json_count);
            adapter.notifyDataSetChanged();
        }
    }
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

//    public String Duratio(String plat,String plong)
//    {
//
//        String dur = new RespToMeeting().calcDur(plat, plong);
//        return dur;
//    }
    private class RespToMeeting extends AsyncTask {

        @Override
        protected Object doInBackground(Object... arg0) {
            return null;
        }


        public String calcDur(String plat,String plong)
        {
            StringBuilder stringBuilder = new StringBuilder();
            Double dist = 0.0;
            String dur = "", dis= "";
            try {
                Log.d("meetinglat", meeting_lat);
                Log.d("meetinglong", meeting_long);
                Log.d("personlat", plat);
                Log.d("personlong", plong);
                String url = "http://maps.googleapis.com/maps/api/directions/json?origin=" + plat + "," + plong + "&destination=" + meeting_lat + "," + meeting_long + "&mode=driving&sensor=false";

                HttpPost httppost = new HttpPost(url);

                HttpClient client = new DefaultHttpClient();
                HttpResponse response;
                stringBuilder = new StringBuilder();


                response = client.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {
            } catch (IOException e) {
            }

            JSONObject jsonObject = new JSONObject();
            try {

                jsonObject = new JSONObject(stringBuilder.toString());
                JSONArray array = jsonObject.getJSONArray("routes");
                JSONObject routes = array.getJSONObject(0);
                JSONArray legs = routes.getJSONArray("legs");
                JSONObject steps = legs.getJSONObject(0);
                JSONObject distance = steps.getJSONObject("distance");
                JSONObject duration = steps.getJSONObject("duration");
                Log.i("Distance", distance.toString());
                Log.i("duration", duration.toString());
                dur = duration.getString("text");
                dis = distance.getString("text");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String text = "\nDist. away from Meeting :" + dis+"\nTime away from Meeting :" + dur;
            // return dist;
            return text;
        }
    }
}
