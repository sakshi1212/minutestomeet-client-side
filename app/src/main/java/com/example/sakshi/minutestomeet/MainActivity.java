//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
    public final static String EXTRA_MESSAGE = "com.sakshi.minutestomeeet.MESSAGE";
    public static final String MY_PREFS_NAME = "MTMPrefsFile";
    String email;
    String password;
    String json_code;
    String json_name;
    String json_msisdn;
    //MySQLiteHelper db;
    //int code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //db = new MySQLiteHelper(this);
        //UserModel existuser = db.getUser(1);
        //existuser.toString();
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        if ((sharedPreferences.contains("flag")) && (sharedPreferences.getBoolean("flag", false))){
                Log.d("test", "flag exists");
                Intent intent = new Intent(this, WelcomeActivity.class);
                //TextView text = (TextView) findViewById(R.id.locationTextView);
                //String message = text.getText().toString();
                intent.putExtra(EXTRA_MESSAGE, "welcome");
                startActivity(intent);

        }else {
            Log.d("test", "flag does not exs");
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.activity_main);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void registerUser (View view){
        Intent intent = new Intent(this, RegisterActivity.class);
        TextView text = (TextView) findViewById(R.id.textViewRegister);
        String message = text.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

    }

    public void loginUser (View view){
        EditText emailET = (EditText)findViewById(R.id.editTextEmail);
        email = emailET.getText().toString();
        EditText passwordET = (EditText)findViewById(R.id.editTextPassword);
        password = passwordET.getText().toString();
        new Connection().execute();
    }
    private class Connection extends AsyncTask {

        @Override
        protected Object doInBackground(Object... arg0) {
            connect();
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
             super.onPostExecute(o);
//            Log.d("Sakshi","Inside Post Execute");
            String error_msg;
            if (Integer.parseInt(json_code)==2) {
                error_msg = "Wrong password entered! ";
                create_invalid_dialog(error_msg);
            }
            else if(Integer.parseInt(json_code)==3){
                error_msg = "Wrong email id entered! ";
                create_invalid_dialog(error_msg);
            }else{
                try{
                   //Insert into SQLite db
                    //db.addUser(new UserModel(email,json_name,json_msisdn));
                    //db.getAllUsers();

                    //Insert into shared preferences
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putBoolean("flag", true);
                    editor.putString("name", json_name);
                    editor.putString("email", email);
                    editor.putString("msisdn", json_msisdn);
                    Log.d("sakshi","inserted into SharedPreferneces");
                    editor.commit();

                    //start intent
                    Intent intent=new Intent(MainActivity.this,WelcomeActivity.class);
                    //TextView text = (TextView) findViewById(R.id.helloworldtextView);
                    //String message = text.getText().toString();
                    intent.putExtra(EXTRA_MESSAGE,"Welcome");
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void connect() {
        // Creating HTTP client
        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse response = null;
        // Creating HTTP Post
        String address = getResources().getString(R.string.IPport)+"/login/reqforlogin";
        HttpPost httpPost = new HttpPost(address);
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
        nameValuePair.add(new BasicNameValuePair("email", email));
        nameValuePair.add(new BasicNameValuePair("password",password));
        // Url Encoding the POST parameters
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            // writing error to Log
            e.printStackTrace();
        }
        try {
            response = httpClient.execute(httpPost);
        } catch (ClientProtocolException e) {
            // writing exception to log
            e.printStackTrace();
        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }

        String responseText = null;
        try {
            responseText = EntityUtils.toString(response.getEntity());
            // Convert String to json object
            JSONObject json = new JSONObject(responseText);
            // get json object if nested
            //JSONObject json_name = json.getJSONObject("name");
            json_name =json.getString("name");
            json_msisdn = json.getString("msisdn");
            json_code =json.getString("code");
//            String json_login =json.getString("login");

//            Log.d("name : ", json_name);
//            Log.d("login : ", json_msisdn);
//            Log.d("code : ",json_code);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void create_invalid_dialog(String error_msg){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setTitle("Invalid Login");
        alertDialogBuilder
                .setMessage(error_msg)
                .setCancelable(false)
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                      dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}


