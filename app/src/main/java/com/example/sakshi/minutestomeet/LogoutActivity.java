//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class LogoutActivity extends Activity {
    public final static String EXTRA_MESSAGE = "com.sakshi.minutestomeeet.MESSAGE";
    public static final String MY_PREFS_NAME = "MTMPrefsFile";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_logout);
        //USING SQLITE
//                MySQLiteHelper db = new MySQLiteHelper(this);
//                Log.d("sakshi", "getting user");
//                UserModel existuser = db.getUser(1);
//                Log.d("sakshi",existuser.toString());
//                Log.d("sakshi", "deleting user");
//                db.deleteUser(existuser);

        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        if ((sharedPreferences.contains("flag")) && (sharedPreferences.getBoolean("flag", false))){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove("flag");
            editor.remove("name");
            editor.remove("email");
            editor.remove("msisdn");
            editor.commit();
            Log.d("sakshi", "preferences deleted");
            //Make toast
            Context context = getApplicationContext();
            CharSequence text = "Logged Out successfully! ";
            int duration = Toast.LENGTH_SHORT;
            Toast.makeText(context, text, duration).show();
            //Back to login screen
            Intent intent=new Intent(this,MainActivity.class);
            intent.putExtra(EXTRA_MESSAGE, "MainActivity");
            startActivity(intent);
        }else{
            Log.d("Sakshi", "ERROR--user not logged in");
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
//    public void backToLogin(){
//        try{
//            Context context = getApplicationContext();
//            CharSequence text = "Logged Out successfully! ";
//            int duration = Toast.LENGTH_SHORT;
//            Toast.makeText(context, text, duration).show();
//            Intent intent=new Intent(this,MainActivity.class);
//            TextView emailtv = (TextView) findViewById(R.id.textViewEmail);
//            String message = emailtv.getText().toString();
//            intent.putExtra(EXTRA_MESSAGE, message);
//            startActivity(intent);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
