//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends Activity {
    public final static String EXTRA_MESSAGE = "com.sakshi.minutestomeeet.MESSAGE";
    String error_message="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_register);
    }

   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void assignDp (View view){
        EditText nameET = (EditText)findViewById(R.id.editTextName);
        String name = nameET.getText().toString();
        EditText emailET = (EditText)findViewById(R.id.editTextEmail);
        String email = emailET.getText().toString();
        EditText passET = (EditText)findViewById(R.id.editTextPassword);
        String pass = passET.getText().toString();
        EditText passrepET = (EditText)findViewById(R.id.editTextRepeatPassword);
        String passrep = passrepET.getText().toString();
        EditText phoneET = (EditText)findViewById(R.id.editTextPhoneNumber);
        String phone = phoneET.getText().toString();
        //Log.d("name", name);
        //Log.d("email", email);
        //Log.d("pass", pass);
        //Log.d("passrep", passrep);
        //Log.d("phone", phone);
        if (email.endsWith(".com")==false){
            error_message="Invalid email entered.";
            create_invalid_dialog();
        }
        else if ((pass.equalsIgnoreCase(passrep))==false){
            error_message="The passwords do not match.";
            create_invalid_dialog();
        }
        else if ((pass.length()<6)){
            error_message = "The password length has to atleast 6.";
            create_invalid_dialog();
        }
        else {
            Intent intent = new Intent(this, assignDp.class);
            Bundle bundle = new Bundle();
            bundle.putString("name", name);
            bundle.putString("email", email);
            bundle.putString("pass", pass);
            bundle.putString("phone", phone);
            intent.putExtras(bundle);
            TextView text = (TextView) findViewById(R.id.buttonNext);
            String message = text.getText().toString();
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
        }
    }
    private void create_invalid_dialog(){
        //Log.d("check", "inside invalid dialog");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setTitle("Input Error");
        alertDialogBuilder
                .setMessage(error_message)
                .setCancelable(false)
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
