//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class One_Location extends FragmentActivity implements OnMapReadyCallback {
    double lat, lon,lat2, lon2;
    protected LocationManager locationManager;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean staleLocationFlag = false;

    //for bundle
    String title;
    int day, month, year, hour, minute, count;
    Bundle toSendBundle;
    String statement= "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_1);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        setTitle("Map");

        String callingActivity = getIntent().getStringExtra("calledfrom");

        if ((callingActivity.equalsIgnoreCase("ARMeeting"))||(callingActivity.equalsIgnoreCase("MyMeetings"))){
            lat = Double.parseDouble(getIntent().getStringExtra("lat"));
            lon = Double.parseDouble(getIntent().getStringExtra("lon"));
            statement="This is the location for the meeting.";
        }else {
            Location loc = displayLocation();
            String strlat = Location.convert(loc.getLatitude(), Location.FORMAT_DEGREES);
            String strlong = Location.convert(loc.getLongitude(), Location.FORMAT_DEGREES);
            lat = Double.valueOf(strlat.trim()).doubleValue();
            lon = Double.valueOf(strlong.trim()).doubleValue();
            statement = "This is the current location of your phone.";
        }
        mapFragment.getMapAsync(this);

        //get values from bundle


    }


    @Override
    public void onMapReady(final GoogleMap map) {
        // Add a marker in Sydney, Australia, and move the camera.
        //LatLng sydney = new LatLng(-34, 151);
        //map.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //  map.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        // Get the location passed to this service through an extra.
        // Sets the map type to be "hybrid"

        LatLng place = new LatLng(lat, lon);

        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(place, 13));

        map.addMarker(new MarkerOptions()
                .title("Location")
                .snippet(statement)
                .position(place));

//        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//            public void onMapClick(LatLng point) {
//                lat2 = point.latitude;
//                lon2 = point.longitude;
//                Toast.makeText(getApplicationContext(),
//                        lat2 + ", " + lon2,
//                        Toast.LENGTH_SHORT).show();
//                LatLng place = new LatLng(lat2, lon2);
//
//                map.setMyLocationEnabled(true);
//                map.moveCamera(CameraUpdateFactory.newLatLngZoom(place, 13));
//                map.clear();
//                map.addMarker(new MarkerOptions()
//                        .title("Location")
//                        .snippet(statement)
//                        .position(place));
//            }
//        });
    }
    public Location displayLocation(){
        Context mContext = getApplicationContext();
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        FallbackLocationTracker fblt;
        if (isGPSEnabled == true){
            Log.d("sakshi", "inside gps enabled");
            fblt = new FallbackLocationTracker(mContext, ProviderLocationTracker.ProviderType.GPS);
        }else if (isNetworkEnabled == true){
            Log.d("sakshi", "inside network enabled");
            fblt = new FallbackLocationTracker(mContext, ProviderLocationTracker.ProviderType.NETWORK);
        }else{
            Log.d("ERROR", "both gps and network not enabled");
            fblt= null;
        }
        Location loc = null;
        fblt.start();
        //boolean a = fblt.hasLocation();
        //Log.d("try",String.valueOf(a));
        if (fblt.hasLocation()){
            staleLocationFlag = true;
            loc = fblt.getLocation();
            // Log.d("loc", loc.toString());
        }else if (fblt.hasPossiblyStaleLocation()){
            staleLocationFlag = true;
            loc = fblt.getPossiblyStaleLocation();
        }

        return loc;


    }
}