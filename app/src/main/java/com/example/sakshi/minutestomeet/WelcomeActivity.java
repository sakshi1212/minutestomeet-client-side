//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class WelcomeActivity extends Activity {
    public final static String EXTRA_MESSAGE = "com.sakshi.minutestomeeet.MESSAGE";
    // Within which the entire activity is enclosed
    DrawerLayout mDrawerLayout;

    // ListView represents Navigation Drawer
    ListView mDrawerList;

    // ActionBarDrawerToggle indicates the presence of Navigation Drawer in the action bar
    ActionBarDrawerToggle mDrawerToggle;

    // Title of the action bar
    String mTitle="";

    //FOR Location:
    protected LocationManager locationManager;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean staleLocationFlag = false;
    String strlat;
    String strlong;
    //FOR location end


    ArrayList<Double> arrLat= new ArrayList<Double>();
    ArrayList<Double> arrLng = new ArrayList<Double>();
    private final int TIME_INTERVAL = 20000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        mTitle = getResources().getString(R.string.app_name);
        getActionBar().setTitle(mTitle);

        //LOCATION --Locking the screen in potrait mode :
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Getting reference to the DrawerLayout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        // Getting reference to the ActionBarDrawerToggle
        mDrawerToggle = new ActionBarDrawerToggle( this,
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.drawer_open,
                R.string.drawer_close){
            /** Called when drawer is closed */
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
//                ((Button) findViewById(R.id.mapButton)).setVisibility(View.VISIBLE);
//                ((TextView) findViewById(R.id.textView4)).setVisibility(View.VISIBLE);
//                ((TextView) findViewById(R.id.locationTextView)).setVisibility(View.VISIBLE);
//                ((TextView) findViewById(R.id.textView5)).setVisibility(View.VISIBLE);
//                ((TextView) findViewById(R.id.localityTextView)).setVisibility(View.VISIBLE);
//                ((TextView) findViewById(R.id.textView6)).setVisibility(View.VISIBLE);
//                ((TextView) findViewById(R.id.areaTextView)).setVisibility(View.VISIBLE);
//                ((TextView) findViewById(R.id.textView8)).setVisibility(View.VISIBLE);
//                ((TextView) findViewById(R.id.countryTextView)).setVisibility(View.VISIBLE);
                invalidateOptionsMenu();
            }
            /** Called when a drawer is opened */
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("Select");
//                ((Button) findViewById(R.id.mapButton)).setVisibility(View.GONE);
//                ((TextView) findViewById(R.id.textView4)).setVisibility(View.GONE);
//                ((TextView) findViewById(R.id.locationTextView)).setVisibility(View.GONE);
//                ((TextView) findViewById(R.id.textView5)).setVisibility(View.GONE);
//                ((TextView) findViewById(R.id.localityTextView)).setVisibility(View.GONE);
//                ((TextView) findViewById(R.id.textView6)).setVisibility(View.GONE);
//                ((TextView) findViewById(R.id.areaTextView)).setVisibility(View.GONE);
//                ((TextView) findViewById(R.id.textView8)).setVisibility(View.GONE);
//                ((TextView) findViewById(R.id.countryTextView)).setVisibility(View.GONE);
                invalidateOptionsMenu();

            }
        };
        // Setting DrawerToggle on DrawerLayout
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        // Creating an ArrayAdapter to add items to the listview mDrawerList
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getBaseContext(),
                R.layout.drawer_list_item ,
                getResources().getStringArray(R.array.nav_drawer_options)
        );
        // Setting the adapter on mDrawerList
        mDrawerList.setAdapter(adapter);
        // Enabling Home button
        getActionBar().setHomeButtonEnabled(true);
        // Enabling Up navigation
        getActionBar().setDisplayHomeAsUpEnabled(true);
        // Setting item click listener for the listview mDrawerList
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view,
                                    int position,
                                    long id) {
                // Getting an array of rivers
                String[] navdraweroptions = getResources().getStringArray(R.array.nav_drawer_options);
                //Currently selected river
                mTitle = navdraweroptions[position];
                // Creating a fragment object
                NavDrawerFragment rFragment = new NavDrawerFragment();
                // Creating a Bundle object
                Bundle data = new Bundle();
                // Setting the index of the currently selected item of mDrawerList
                data.putInt("position", position);
                // Setting the position to the fragment
                rFragment.setArguments(data);
                // Getting reference to the FragmentManager
                FragmentManager fragmentManager = getFragmentManager();
                // Creating a fragment transaction
                FragmentTransaction ft = fragmentManager.beginTransaction();
                // Adding a fragment to the fragment transaction
                ft.replace(R.id.content_frame, rFragment);
                // Committing the transaction
                ft.commit();
                // Closing the drawer
                 mDrawerLayout.closeDrawer(mDrawerList);
                ((TextView) findViewById(R.id.textView4)).setVisibility(View.GONE);
                ((TextView) findViewById(R.id.locationTextView)).setVisibility(View.GONE);
                ((TextView) findViewById(R.id.textView5)).setVisibility(View.GONE);
                ((TextView) findViewById(R.id.localityTextView)).setVisibility(View.GONE);
                ((TextView) findViewById(R.id.textView6)).setVisibility(View.GONE);
                ((TextView) findViewById(R.id.areaTextView)).setVisibility(View.GONE);
                ((TextView) findViewById(R.id.textView8)).setVisibility(View.GONE);
                ((TextView) findViewById(R.id.countryTextView)).setVisibility(View.GONE);
                ((Button) findViewById(R.id.mapButton)).setVisibility(View.GONE);
            }
        });



        Intent intent = new Intent(WelcomeActivity.this, LocationUpdate.class);
        PendingIntent pintent = PendingIntent.getService(WelcomeActivity.this, 0, intent, 0);
        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000, pintent);

        startNotify();

        Location loc = displayLocation();
        String strloc = Location.convert(loc.getLatitude(), Location.FORMAT_DEGREES) + "-lat  "
                + Location.convert(loc.getLongitude(), Location.FORMAT_DEGREES)+"-long";
        TextView loctv = (TextView)findViewById(R.id.locationTextView);
        loctv.setText(strloc);
//            staletv.setVisibility(View.VISIBLE);
//        }
        strlat=Location.convert(loc.getLatitude(), Location.FORMAT_DEGREES);
        strlong=Location.convert(loc.getLongitude(), Location.FORMAT_DEGREES);
        Context mContext = getApplicationContext();
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
            String locality = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            //String zip = addresses.get(0).getPostalCode();
            String country = addresses.get(0).getCountryName();
            if (locality!=null){
                TextView localitytv = (TextView) findViewById(R.id.localityTextView);
                localitytv.setText(locality);
            }
            if (state!=null) {
                TextView areatv = (TextView) findViewById(R.id.areaTextView);
                areatv.setText(state);
            }
            if(country!=null){
                TextView counttv = (TextView)findViewById(R.id.countryTextView);
                counttv.setText(country);
            }

//           Log.d("city", locality);
//            Log.d("state", state);
//         //Log.d("zip", zip);
//           Log.d("country", country);
        }catch(Exception e){
            e.printStackTrace();
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        double latitude = intent.getDoubleExtra(LocationUpdate.EXTRA_LATITUDE, 0);
                        double longitude = intent.getDoubleExtra(LocationUpdate.EXTRA_LONGITUDE, 0);

                        //latitude = round(latitude, 4);
                        //longitude = round(longitude, 4);
                        String strloc = latitude + "-lat  "
                                + longitude+"-long";
                        TextView loctv = (TextView)findViewById(R.id.locationTextView);
                        loctv.setText(strloc);
                        Context mContext = getApplicationContext();
                        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
                        try {
                            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
                            String locality = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            //String zip = addresses.get(0).getPostalCode();
                            String country = addresses.get(0).getCountryName();
                            if (locality!=null){
                                TextView localitytv = (TextView) findViewById(R.id.localityTextView);
                                localitytv.setText(locality);
                            }
                            if (state!=null) {
                                TextView areatv = (TextView) findViewById(R.id.areaTextView);
                                areatv.setText(state);
                            }
                            if(country!=null){
                                TextView counttv = (TextView)findViewById(R.id.countryTextView);
                                counttv.setText(country);
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                }, new IntentFilter(LocationUpdate.ACTION_LOCATION_BROADCAST)
        );

    }
    private void startNotify() {

        Intent intent = new Intent(WelcomeActivity.this, NotificationService.class);
        PendingIntent pintent = PendingIntent.getService(WelcomeActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 14400000, pintent);

    }

    public void showInMap(View view){
        // strlat, strlong
        Intent maps = new Intent(WelcomeActivity.this, One_Location.class);
        maps.putExtra("calledfrom" , "WelcomeActivity");
        maps.putExtra("lat",strlat);
        maps.putExtra("lon", strlong);
        WelcomeActivity.this.startActivity(maps);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }



    /** Handling the touch event of app icon */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId()==R.id.action_settings) {
                Intent intent = new Intent(this, sendFeedbackActivity.class);
                intent.putExtra(EXTRA_MESSAGE, "sendFeedback");
                startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /** Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
            // If the drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
        public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public Location displayLocation(){
        Context mContext = getApplicationContext();
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        FallbackLocationTracker fblt;
        if (isGPSEnabled == true){
            Log.d("sakshi", "inside gps enabled");
            fblt = new FallbackLocationTracker(mContext, ProviderLocationTracker.ProviderType.GPS);
        }else if (isNetworkEnabled == true){
            Log.d("sakshi", "inside network enabled");
            fblt = new FallbackLocationTracker(mContext, ProviderLocationTracker.ProviderType.NETWORK);
        }else{
            Log.d("ERROR", "both gps and network not enabled");
            fblt= null;
        }
        Location loc = null;
        fblt.start();
        //boolean a = fblt.hasLocation();
        //Log.d("try",String.valueOf(a));
        if (fblt.hasLocation()){
            staleLocationFlag = true;
            loc = fblt.getLocation();
           // Log.d("loc", loc.toString());
        }else if (fblt.hasPossiblyStaleLocation()){
            staleLocationFlag = true;
            loc = fblt.getPossiblyStaleLocation();
        }

        return loc;


    }

    public void newMeeting2(View view){
        EditText titleET = (EditText)findViewById(R.id.titleEditText);
        String title = titleET.getText().toString();
        DatePicker datePicker = (DatePicker)findViewById(R.id.datePicker);
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth() + 1;
        int year = datePicker.getYear();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putInt("day", day);
        bundle.putInt("month", month);
        bundle.putInt("year", year);
        Log.d("sakshi", title + day + month + year);
        Intent intent = new Intent(this, NewMeeting2.class);
        intent.putExtras(bundle);
        intent.putExtra(EXTRA_MESSAGE, "new Meeting 2");
        startActivity(intent);
    }
}
