//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sakshi on 12/3/2015.
 */
public class NavDrawerFragment extends Fragment {
    //MySQLiteHelper db;
    public final static String EXTRA_MESSAGE = "com.sakshi.minutestomeeet.MESSAGE";
    public static final String MY_PREFS_NAME = "MTMPrefsFile";
    String json_count;
    ArrayList<String> reqlistItems=new ArrayList<String>();
    ArrayList<String> myMeetingsListItems=new ArrayList<String>();
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> myMeetingsAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Retrieving the currently selected item number
        int position = getArguments().getInt("position");

        // List of option
        String[] navdraweroptions = getResources().getStringArray(R.array.nav_drawer_options);
        View v = null;
        Log.d("selected", Integer.toString(position));
        if (position == 0){//Home selected
            Intent intent = new Intent(getActivity(), WelcomeActivity.class);
            intent.putExtra(EXTRA_MESSAGE, "welcome");
            startActivity(intent);
          //TODO : try and make this better if there is time
        }else if (position == 4){//Logout selected
            Log.d("user", "logging user out ");
            Intent intent = new Intent(getActivity(), LogoutActivity.class);
            intent.putExtra(EXTRA_MESSAGE, "logout");
            startActivity(intent);
        }else if (position == 1) {//new meeting selected
            Log.d("user", "new meeting selected  ");
            v = inflater.inflate(R.layout.fragment_new_meeting, container, false);
        }else if (position == 2) {//meeting requests selected
            Log.d("user", "meeting requests selected ");
            v = inflater.inflate(R.layout.fragment_meeting_requests, container, false);
            LinearLayout llfmr =(LinearLayout)v.findViewById(R.id.llfmr);
            ListView list = (ListView)llfmr.findViewById(R.id.meetingReqListView);
            adapter=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                    reqlistItems);
            list.setAdapter(adapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                     String item = ((TextView)view).getText().toString();
                   // String data = (String) parent.getItemAtPosition(position);
                    Intent meeting = new Intent(getActivity(), ARMeeting.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("id", item);
                    meeting.putExtras(bundle);
                    startActivity(meeting);
                    //Toast.makeText(getActivity(), item, Toast.LENGTH_LONG).show();

                }
            });
            reqMeetingsReqFromServer();
        }else if (position == 3) {//My meetings is selected
            Log.d("user", "my meetings selected");
            v = inflater.inflate(R.layout.fragment_meeting_requests, container, false);
            LinearLayout llfmr =(LinearLayout)v.findViewById(R.id.llfmr);
            ListView list = (ListView)llfmr.findViewById(R.id.meetingReqListView);
            myMeetingsAdapter=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                    myMeetingsListItems);
            list.setAdapter(myMeetingsAdapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String item = ((TextView) view).getText().toString();
                    // String data = (String) parent.getItemAtPosition(position);
                    Intent meeting = new Intent(getActivity(), MyMeetingsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("id", item);
                    meeting.putExtras(bundle);
                    startActivity(meeting);
                    //Toast.makeText(getActivity(), item, Toast.LENGTH_LONG).show();
                }
            });
            reqMyMeetingsFromServer();
        }


        else {
            // Creating view correspoding to the fragment
            v = inflater.inflate(R.layout.fragment_layout, container, false);

            // Getting reference to the TextView of the Fragment
            TextView tv = (TextView) v.findViewById(R.id.tv_content);

            // Setting currently selected river name in the TextView
            tv.setText(navdraweroptions[position]);
        }
        // Updating the action bar title
        getActivity().getActionBar().setTitle(navdraweroptions[position]);

        return v;
    }
    public void reqMyMeetingsFromServer(){
        new MyMeetings().execute();
    }
    private class MyMeetings extends AsyncTask {
        protected Object doInBackground(Object... arg0) {
            // Creating HTTP client
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = null;
            // Creating HTTP Post
            String address = getResources().getString(R.string.IPport) + "/meeting/my_meetings";
            HttpPost httpPost = new HttpPost(address);
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(MY_PREFS_NAME, 0);
            String name = (sharedPreferences.getString("name", ""));
            String msisdn = (sharedPreferences.getString("msisdn", ""));
            nameValuePair.add(new BasicNameValuePair("name", name));
            nameValuePair.add(new BasicNameValuePair("msisdn", msisdn));
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
            } catch (UnsupportedEncodingException e) {
                // writing error to Log
                e.printStackTrace();
            }
            try {
                response = httpClient.execute(httpPost);
            } catch (ClientProtocolException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (IOException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            String responseText = null;
            try {
                responseText = EntityUtils.toString(response.getEntity());
                // Convert String to json object
                JSONObject json = new JSONObject(responseText);
                // get json object if nested
                //JSONObject json_name = json.getJSONObject("name");
                json_count = json.getString("count");
                Log.d("YYAYYAYA", json_count);
                int count = Integer.parseInt(json_count);
                if (count != 0) {
                    for (int i = 1; i <= count; i++) {
                        String a = Integer.toString(i);
                        myMeetingsListItems.add("Meeting ID --" + Integer.toString(Integer.parseInt(json.getString(a)) + 10000));
                    }
                } else {
                    myMeetingsListItems.add("No Meetings");
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            myMeetingsAdapter.notifyDataSetChanged();
        }
    }
    public void reqMeetingsReqFromServer(){
        new Connection().execute();
    }
    private class Connection extends AsyncTask {

        @Override
        protected Object doInBackground(Object... arg0) {
            // Creating HTTP client
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = null;
            // Creating HTTP Post
            String address = getResources().getString(R.string.IPport)+"/meeting/meeting_req";
            HttpPost httpPost = new HttpPost(address);
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(MY_PREFS_NAME, 0);
            String Iname = (sharedPreferences.getString("name", ""));
            String Imsisdn = (sharedPreferences.getString("msisdn", ""));
            nameValuePair.add(new BasicNameValuePair("name",Iname ));
            nameValuePair.add(new BasicNameValuePair("msisdn", Imsisdn));
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
            } catch (UnsupportedEncodingException e) {
                // writing error to Log
                e.printStackTrace();
            }
            try {
                response = httpClient.execute(httpPost);
            } catch (ClientProtocolException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (IOException e) {
                // writing exception to log
                e.printStackTrace();
            }catch (Exception e) {
                e.printStackTrace();
            }


            String responseText = null;
            try {
                responseText = EntityUtils.toString(response.getEntity());
                // Convert String to json object
                JSONObject json = new JSONObject(responseText);
                // get json object if nested
                //JSONObject json_name = json.getJSONObject("name");
                json_count =json.getString("count");
                int count = Integer.parseInt(json_count);
                if (count != 0){
                    for (int i = 1; i<=count; i++){
                        String a = Integer.toString(i);
                        reqlistItems.add("Meeting ID --"+Integer.toString(Integer.parseInt(json.getString(a)) + 10000));
                    }
                }else {
                    reqlistItems.add("No Pending Meeting Requests");
                }
//            Log.d("code : ",json_code);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            adapter.notifyDataSetChanged();
        }
    }
}
