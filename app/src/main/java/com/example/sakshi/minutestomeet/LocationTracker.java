//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.location.Location;

/**
 * Created by Sakshi on 12/5/2015.
 */
public interface LocationTracker {
    public interface LocationUpdateListener{
        public void onUpdate(Location oldLoc, long oldTime, Location newLoc, long newTime);
    }
    public void start();
    public void start(LocationUpdateListener update);
    public void stop();
    public boolean hasLocation();
    public boolean hasPossiblyStaleLocation();
    public Location getLocation();
    public Location getPossiblyStaleLocation();

}