//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

public class NotificationService extends Service {

    private final static String TAG = "ShowNotification";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Intent mainIntent = new Intent(this, WelcomeActivity.class);

        NotificationManager notificationManager
                = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification noti = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentIntent(PendingIntent.getActivity(this, 0, mainIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT))
                .setContentTitle("Minutes to Meet")
                .setContentText("You have pending meeting requests")
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("Minutes to Meet")
             //   .setWhen(System.currentTimeMillis())
                .build();

        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String ringPreference = preference.getString("Ringtone", "DEFAULT_SOUND");

        noti.defaults |= Notification.DEFAULT_SOUND;
        noti.flags  |= Notification.FLAG_AUTO_CANCEL;
        noti.sound = Uri.parse(ringPreference);
        notificationManager.notify(0, noti);

        Log.i(TAG, "Notification created");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
}



