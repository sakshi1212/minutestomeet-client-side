//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class viewCompleteProfile extends Activity {
    public final static String EXTRA_MESSAGE = "com.sakshi.minutestomeeet.MESSAGE";
    public static final String MY_PREFS_NAME = "MTMPrefsFile";
    String name, email, pass, phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_view_complete_profile);
        Bundle bundle = getIntent().getExtras();
        name = bundle.getString("name");
        email = bundle.getString("email");
        //pass = bundle.getString("pass");
        phone = bundle.getString("phone");
        TextView nametv=(TextView)findViewById(R.id.nameTextView);
        nametv.setText(name);
        TextView emailtv=(TextView)findViewById(R.id.emailTextView);
        emailtv.setText(email);
        TextView msisdntv=(TextView)findViewById(R.id.msisdnTextView);
        msisdntv.setText(phone);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_complete_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
     public void welcomeScreen (View view) {
         //put in shared preferences
         SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
         editor.putBoolean("flag", true);
         editor.putString("name", name);
         editor.putString("email", email);
         editor.putString("msisdn", phone);
         Log.d("sakshi", "inserted into SharedPreferneces");
         editor.commit();
         Intent intent=new Intent(this,WelcomeActivity.class);
         //TextView text = (TextView) findViewById(R.id.helloworldtextView);
         //String message = text.getText().toString();
         intent.putExtra(EXTRA_MESSAGE,"Welcome");
         startActivity(intent);
     }
}
