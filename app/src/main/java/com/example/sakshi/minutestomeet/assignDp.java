//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class assignDp extends Activity {
    //private static int RESULT_LOAD_IMG = 1;
    //String imgDecodableString;
    private static final int SELECT_PICTURE = 1;
    private String selectedImagePath;
    public final static String EXTRA_MESSAGE = "com.sakshi.minutestomeeet.MESSAGE";
    String name, pass, email, phone;
    int SELECT_FILE=1,REQUEST_CAMERA=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_assign_dp);
        //getting values from previous activity
        Bundle bundle = getIntent().getExtras();
        name = bundle.getString("name");
        email = bundle.getString("email");
        pass = bundle.getString("pass");
        phone = bundle.getString("phone");
//        Log.d("name", name);
//        Log.d("email", email);
//        Log.d("pass", pass);
//        Log.d("phone", phone);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_assign_dp, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void openGallery(View v)
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FILE);

    }

    public void useCamera(View v)
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ImageView ivImage = (ImageView) findViewById(R.id.imageViewSelectedDp);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String selectedImagePath = getPath(selectedImageUri);
                System.out.println("Image Path : " + selectedImagePath);

                ivImage.setImageURI(selectedImageUri);
                ivImage.setVisibility(View.VISIBLE);
                TextView textViewString = (TextView) findViewById(R.id.textViewString1);
                textViewString.setVisibility(View.VISIBLE);
                Button buttonViewCompleteProfile = (Button) findViewById(R.id.buttonViewCompleteProfile);
                buttonViewCompleteProfile.setVisibility(View.VISIBLE);
            }

            else  if (requestCode == REQUEST_CAMERA) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ivImage.setImageBitmap(thumbnail);
                ivImage.setVisibility(View.VISIBLE);
                TextView textViewString = (TextView) findViewById(R.id.textViewString1);
                textViewString.setVisibility(View.VISIBLE);
                Button buttonViewCompleteProfile = (Button) findViewById(R.id.buttonViewCompleteProfile);
                buttonViewCompleteProfile.setVisibility(View.VISIBLE);
            }
        }
    }
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    public void useDefaultImage (View view) {
        TextView textViewString = (TextView) findViewById(R.id.textViewString1);
        textViewString.setVisibility(View.VISIBLE);
        ImageView imgViewSelectedDp = (ImageView) findViewById(R.id.imageViewSelectedDp);
        imgViewSelectedDp.setImageResource(R.drawable.defaultdp);
        imgViewSelectedDp.setVisibility(View.VISIBLE);
        Button buttonViewCompleteProfile = (Button) findViewById(R.id.buttonViewCompleteProfile);
        buttonViewCompleteProfile.setVisibility(View.VISIBLE);
    }


    public void viewCompleteProfile(View view) {

        new Connection().execute();
    }

    private class Connection extends AsyncTask {
        String json_code, json_msg, json_name, json_email, json_msisdn;
        @Override
        protected Object doInBackground(Object... arg0) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = null;
            String address = getResources().getString(R.string.IPport)+"/registrations";
            HttpPost httpPost = new HttpPost(address);
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
            nameValuePair.add(new BasicNameValuePair("name", name));
            nameValuePair.add(new BasicNameValuePair("email", email));
            nameValuePair.add(new BasicNameValuePair("password", pass));
            nameValuePair.add(new BasicNameValuePair("msisdn", phone));
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
            } catch (UnsupportedEncodingException e) {
                // writing error to Log
                e.printStackTrace();
            }
            try {
                response = httpClient.execute(httpPost);
            } catch (ClientProtocolException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (IOException e) {
                // writing exception to log
                e.printStackTrace();
            }catch (Exception e) {
                e.printStackTrace();
            }

            String responseText = null;
            try {
                responseText = EntityUtils.toString(response.getEntity());
                // Convert String to json object
                JSONObject json = new JSONObject(responseText);
                // get json object if nested
                //JSONObject json_name = json.getJSONObject("name");
                json_code =json.getString("code");
                json_msg =json.getString("msg");
                json_name =json.getString("name");
                json_email =json.getString("email");
                json_msisdn =json.getString("msisdn");
                Log.d("code : ",json_code);
                Log.d("code : ", json_email);
                Log.d("code : ",json_msg);
                Log.d("code : ",json_email);
                Log.d("code : ",json_msisdn);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            Log.d("sakshi", "inside post execute");
            if (Integer.parseInt(json_code)==0){
                String error_msg = "Internal server error. Try again later.";
                create_invalid_dialog(error_msg);
            }
            else if (Integer.parseInt(json_code)==2){
                Log.d("sakshi","inside code 2");
                String error_msg = "Email id / Phone number already Exists.";
                create_invalid_dialog(error_msg);
            }
            else {
                Log.d("check", "inside else ");
                Intent intent = new Intent(assignDp.this, viewCompleteProfile.class);
                Bundle bundle = new Bundle();
                bundle.putString("code", json_code);
                bundle.putString("email", json_email);
                bundle.putString("name", json_name);
                bundle.putString("phone", json_msisdn);
                intent.putExtras(bundle);
                TextView text = (TextView) findViewById(R.id.buttonViewCompleteProfile);
                String message = text.getText().toString();
                intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        }

    }
    private void create_invalid_dialog(String error_msg){
        //Log.d("check", "inside invalid dialog");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setTitle("Input Error");
        alertDialogBuilder
                .setMessage(error_msg)
                .setCancelable(false)
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
