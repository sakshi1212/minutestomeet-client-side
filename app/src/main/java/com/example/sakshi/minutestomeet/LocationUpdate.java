//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;
import android.app.Service;
        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.location.Location;
        import android.location.LocationManager;
        import android.os.AsyncTask;
        import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

        import org.apache.http.HttpResponse;
        import org.apache.http.NameValuePair;
        import org.apache.http.client.ClientProtocolException;
        import org.apache.http.client.HttpClient;
        import org.apache.http.client.entity.UrlEncodedFormEntity;
        import org.apache.http.client.methods.HttpPost;
        import org.apache.http.impl.client.DefaultHttpClient;
        import org.apache.http.message.BasicNameValuePair;
        import org.apache.http.util.EntityUtils;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.IOException;
        import java.io.UnsupportedEncodingException;
        import java.util.ArrayList;
        import java.util.List;

public class LocationUpdate extends Service {

    protected LocationManager locationManager;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean staleLocationFlag = false;
    String strlat,json_code;
    public static final String MY_PREFS_NAME = "MTMPrefsFile";
    String strlong;
    public static final String
            ACTION_LOCATION_BROADCAST = LocationUpdate.class.getName() + "LocationBroadcast",
            EXTRA_LATITUDE = "extra_latitude",
            EXTRA_LONGITUDE = "extra_longitude";
    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Location", "FirstService Started");
        Location loc = displayLocation();
        String strloc = Location.convert(loc.getLatitude(), Location.FORMAT_DEGREES) + "-lat  "
                + Location.convert(loc.getLongitude(), Location.FORMAT_DEGREES)+"-long";

//            staletv.setVisibility(View.VISIBLE);
//        }
        sendBroadcastMessage(loc);
        strlat=Location.convert(loc.getLatitude(), Location.FORMAT_DEGREES);
        strlong=Location.convert(loc.getLongitude(), Location.FORMAT_DEGREES);
        Log.e("strlat",strlat);
        Log.e("strlong",strlong);

        new UpdateLoc().execute();

        return super.onStartCommand(intent, flags, startId);
    }

    public Location displayLocation(){
        Context mContext = getApplicationContext();

        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        FallbackLocationTracker fblt;
        if (isGPSEnabled == true){
            Log.d("sakshi", "inside gps enabled");
            fblt = new FallbackLocationTracker(mContext, ProviderLocationTracker.ProviderType.GPS);
        }else if (isNetworkEnabled == true){
            Log.d("sakshi", "inside network enabled");
            fblt = new FallbackLocationTracker(mContext, ProviderLocationTracker.ProviderType.NETWORK);
        }else{
            Log.d("ERROR", "both gps and network not enabled");
            fblt= null;
        }
        Location loc = null;
        fblt.start();
        //boolean a = fblt.hasLocation();
        //Log.d("try",String.valueOf(a));
        if (fblt.hasLocation()){
            staleLocationFlag = true;
            loc = fblt.getLocation();
            // Log.d("loc", loc.toString());
        }else if (fblt.hasPossiblyStaleLocation()){
            staleLocationFlag = true;
            loc = fblt.getPossiblyStaleLocation();
        }

        return loc;


    }


    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.d("Location", "FirstService destroyed");
    }


    private void sendBroadcastMessage(Location location) {
        if (location != null) {
            Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
            intent.putExtra(EXTRA_LATITUDE, location.getLatitude());
            intent.putExtra(EXTRA_LONGITUDE, location.getLongitude());
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }
    private class UpdateLoc extends AsyncTask {

        @Override
        protected Object doInBackground(Object... arg0) {
            // Creating HTTP client
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = null;
            // Creating HTTP Post
            String address = getResources().getString(R.string.IPport) + "/location/update_location";
            HttpPost httpPost = new HttpPost(address);

            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
            nameValuePair.add(new BasicNameValuePair("lat", strlat));
            nameValuePair.add(new BasicNameValuePair("long", strlong));
            SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            nameValuePair.add(new BasicNameValuePair("msisdn", (sharedPreferences.getString("msisdn", ""))));
            nameValuePair.add(new BasicNameValuePair("name", (sharedPreferences.getString("name", ""))));
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
            } catch (UnsupportedEncodingException e) {
                // writing error to Log
                e.printStackTrace();
            }
            try {
                response = httpClient.execute(httpPost);
            } catch (ClientProtocolException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (IOException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String responseText = null;
            try {
                responseText = EntityUtils.toString(response.getEntity());
                // Convert String to json object
                JSONObject json = new JSONObject(responseText);
                // get json object if nested
                //JSONObject json_name = json.getJSONObject("name");
                json_code = json.getString("code");

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            Log.d("RESPONSE CODE ", json_code);

        }
    }
}


