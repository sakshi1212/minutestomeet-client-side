//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.provider.ContactsContract.Contacts;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class NewMeeting3 extends Activity {
    private static final int CONTACT_PICKER_RESULT = 1001;
    ArrayList<String> listItems=new ArrayList<String>();
    ArrayAdapter<String> adapter;
    Bundle bundle;
    String title;
    int day, month, year, hour, minute;

    int count = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_meeting3);
        ListView list = (ListView)findViewById(R.id.listView);
        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                listItems);
        list.setAdapter(adapter);

        //get values from Bundle
       bundle = getIntent().getExtras();
//        title = bundle.getString("title");
//        day = bundle.getInt("day");
//        month = bundle.getInt("month");
//        year = bundle.getInt("year");
//        hour = bundle.getInt("hour");
//        minute = bundle.getInt("minute");
//        Log.d("sakshi title ", title);
//        Log.d("sakshi hour", Integer.toString(hour));
//        //toSendBundle
//        toSendBundle = new Bundle();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_meeting3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void doLaunchContactPicker(View view) {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                Contacts.CONTENT_URI);
        startActivityForResult(contactPickerIntent, CONTACT_PICKER_RESULT);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CONTACT_PICKER_RESULT:
                    Bundle extras = data.getExtras();
                    Set keys = extras.keySet();
                    Iterator iterate = keys.iterator();
                    while (iterate.hasNext()) {
                        String key = iterate.next().toString();
                        Log.v("debug", key + "[" + extras.get(key) + "]");
                    }
                    Uri result = data.getData();
                    Log.v("debug", "Got a result: " + result.toString());
                    String id = result.getLastPathSegment();
                    Log.d("sakshi--id",id);
                    Cursor cursor = getContentResolver().query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    if (cursor.moveToFirst()) {
                        int msisdnIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
                        String msisdn = cursor.getString(msisdnIdx);
                        int displaynameIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                        String displayname = cursor.getString(displaynameIdx);
                       // Log.v("SAKSHI-MSISDN", "Got name: " + displayname);
                       // Log.v("SAKSHI-MSISDN", "Got msisdn: " + msisdn);
                        Button selconbutton = (Button)findViewById(R.id.selectContactsButton);
                        selconbutton.setText("Select More Contacts");
                        listItems.add(displayname + " --> " + msisdn);
                        adapter.notifyDataSetChanged();
                        //setting contacts in bundle
                        count ++;
                        bundle.putString("name"+count, displayname);
                        bundle.putString("msisdn"+count, msisdn);

                       }
                    break;
            }

        } else { //if user cancels the operation or if something else goes wrong
            // gracefully handle failure
            Log.w("sakshi", "Warning: activity result not ok");
        }

    }

    public void setMeetingLocation(View view){
        Intent maps = new Intent(NewMeeting3.this, MapsActivity.class);
        bundle.putInt("count", count);
        maps.putExtras(bundle);
        NewMeeting3.this.startActivity(maps);

    }

}
