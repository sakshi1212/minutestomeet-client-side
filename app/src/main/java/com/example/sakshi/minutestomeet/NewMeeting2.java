//# CSIT 6000B    #  SAKSHI KAKKAR        20299114          skakkar@connect.ust.hk
package com.example.sakshi.minutestomeet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

public class NewMeeting2 extends Activity {
    public final static String EXTRA_MESSAGE = "com.sakshi.minutestomeeet.MESSAGE";
    String title;
    int day, month, year;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_meeting2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_meeting2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void newMeeting3(View view){
        TimePicker timePicker = (TimePicker)findViewById(R.id.timePicker);
        timePicker.clearFocus();
        int hour = timePicker.getCurrentHour();
        int minute = timePicker.getCurrentMinute();
        Log.d("sakshi", "Time :" + hour + minute);
        //add in existing bundle
        Bundle bundle = getIntent().getExtras();
        bundle.putInt("hour", hour);
        bundle.putInt("minute", minute);
        //start intent
        Intent intent = new Intent(this, NewMeeting3.class);
        intent.putExtras(bundle);
        intent.putExtra(EXTRA_MESSAGE, "new Meeting 3");
        startActivity(intent);

    }

}
