
MinutesToMeet is a Native Android Application. 

This application is used to schedule meetings between users. Each meeting will have a time, place and participant details associated to it. Users can accept or decline meetings requests. 

Apart from regularly scheduling meetings, this application will keep track of where each user is before the meeting. The application will keep track of the phone's position and register how much time the user will take to the meeting location. 


Further Documentation : https://drive.google.com/file/d/0B5BkttoARfGUQ0N5alRrdlBQWnM/view?usp=sharing